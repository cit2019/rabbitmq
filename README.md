# RabbitMQ

## Docker images

All images are built from Dockerfiles found in the [images](https://gitlab.com/cit2019/rabbitmq/tree/master/images) directory and pushed to the repository's [container registry](https://gitlab.com/cit2019/rabbitmq/container_registry).

### cit2019/rabbitmq

Docker image based on the [official RabbitMQ Docker Image](https://github.com/docker-library/rabbitmq/blob/facda0977005887a4b42dd06f3f8eac8f6cf7169/3.8/ubuntu/Dockerfile), with the source image set to [baleanalib/raspberrypi3 Docker Image](https://hub.docker.com/r/balenalib/raspberrypi3) (in order to be compatible with ARM architecture).

### cit2019/rabbitmq/management

Docker image based on the [official RabbitMQ Management Docker Image](https://github.com/docker-library/rabbitmq/blob/af5f6ff9a3916d89be6d190d562d247ae12ffa73/3.8/ubuntu/management/Dockerfile), built on top of [cit2019/rabbitmq](https://gitlab.com/cit2019/rabbitmq/tree/master/images/rabbitmq) (in order to be compatible with ARM architecture).

### cit2019/rabbitmq/release

Docker image based on [cit2019/rabbitmq/management](https://gitlab.com/cit2019/rabbitmq/tree/master/images/management) with configuration for Consul clustering.

### cit2019/rabbitmq/release-amd64

Docker image based on the [official RabbitMQ Management Docker Image](https://github.com/docker-library/rabbitmq/blob/af5f6ff9a3916d89be6d190d562d247ae12ffa73/3.8/ubuntu/management/Dockerfile), used for test purposes on an AMD64 machine.

## Stacks

All stacks are defined in the [Compose file version 3](https://docs.docker.com/compose/compose-file/) format in the [stacks](https://gitlab.com/cit2019/rabbitmq/tree/master/stacks) directory. 3 stacks are available :
- [release](https://gitlab.com/cit2019/rabbitmq/blob/master/stacks/release/docker-compose.yml) - the final stack for Raspberry Pi
- [release_amd64](https://gitlab.com/cit2019/rabbitmq/blob/master/stacks/release_amd64/docker-compose.yml) - the final stack for AMD64 architecture, used for local development
- [management](https://gitlab.com/cit2019/rabbitmq/blob/master/stacks/management/docker-compose.yml) - a simple stack with only one instance of cit2019/rabbitmq/management for test purposes


### consul

[Consul](https://www.consul.io/intro/index.html) is used for service discovery of the different nodes. Only one instance is created since the service is only necessary for discovery and health checks. On failure, a new instance is spawned.

The **Consul Management Interface** is available on port **8005**.

### rabbitmq

This is the RabbitMQ cluster, deployed as a `global` service (the service is deployed on every node), with ports bound in `host` mode (the ports are bound on every node, not only the first one), with a `rabbitmq-data` volume for data persistance (the volume is NOT shared between instances of the service).  

The **RabbitMQ Management Interface** is available on port **15672**.

#### Load balancing

As described [here](https://blog.scottlogic.com/2016/08/30/docker-1-12-swarm-mode-round-robin.html), there is no need for a load balancer since Swarm offers it already - no matter which node the request is send to, Swarm will spread the load accross all nodes.

## Deployment

```bash
docker network create --driver=overlay --attachable rabbitmq-external
docker stack deploy --resolve-image=never --compose-file stack.yml rabbit
```

The whole stack is deployed on the Swarm through the compose file. The `--resolve-image=never` flag is used to avoid the `no suitable node` error caused by an incorrect definition of the architecture metadata for the image (AMD64 instead of ARM).

## Usefull commands

- `docker node ls` - list available nodes in the Swarm
- `docker stack ps <stack name>` - list the tasks in the stack
- `docker stack services <stack name>` - list the services in the stack
- `docker service logs <service name>` - fetch the logs of a service or task
- `docker build -t <image name in Container Registry> .` - build the image from a Dockerfile present in the current directory
- `docker push <image name in Container Registry>` - push the newly-built image to the container registry

## References

<https://developervisits.wordpress.com/2018/08/28/creating-dynamic-rabbitmq-cluster-with-consul-discovery-in-docker/>

<https://github.com/rabbitmq/rabbitmq-server/issues/1454>

<https://medium.com/hepsiburadatech/implementing-highly-available-rabbitmq-cluster-on-docker-swarm-using-consul-based-discovery-45c4e7919634>

<https://blog.scottlogic.com/2016/08/30/docker-1-12-swarm-mode-round-robin.html>
